# FlyNavigation

```groovy
allprojects {
    repositories {
        google()
        mavenCentral()

        //或者 sonatype
        maven { url "https://s01.oss.sonatype.org/content/groups/public" }
        maven { url "https://s01.oss.sonatype.org/content/repositories/releases" }
    }
}
```

### fly-navigation（必需）

```groovy
implementation 'com.gitee.tiamosu:fly-navigation:1.7.1'
```

### fly-databinding（可选）

```groovy
implementation 'com.gitee.tiamosu:fly-databinding:1.7.1'
```

## 如何使用

### fly-navigation 模块

#### 1、继承

```kotlin
abstract class BaseActivity : FlySupportActivity() 
```

```kotlin
abstract class BaseFragment : FlySupportFragment() 
```

#### 2、Fragment特性

生命周期调用顺序：`onActivityCreated()` -> `onResume()` -> `onFlySupportVisible()` -> `onFlyLazyInitView()`
-> `onPause()` -> `onFlySupportInvisible()`

##### A、对用户可见

```kotlin
override fun onFlySupportVisible() {
    super.onFlySupportVisible()
    Log.e(fragmentTag, "onFlySupportVisible")
}
```

##### B、对用户不可见

```kotlin
override fun onFlySupportInvisible() {
    super.onFlySupportInvisible()
    Log.e(fragmentTag, "onFlySupportInvisible")
}
```

##### C、Fragment懒加载

该方法会在页面可见并且转场动画结束后调用，保证Fragment动画的流畅。

```kotlin
 override fun onFlyLazyInitView() {
    super.onFlyLazyInitView()
    Log.e(fragmentTag, "onFlyLazyInitView")
}

//备用，于 onFlyLazyInitView 之后
override fun onFlyLazyInitView2() {
    super.onFlyLazyInitView2()
    Log.e(fragmentTag, "onFlyLazyInitView2")
}
```

##### D、Fragment对于Back键监听

 ```kotlin
  override fun onBackPressedSupport(): Boolean {
    //do something
    return true
}
 ```

#### 3、扩展

- [FlyNavigationExt](https://gitee.com/tiamosu/FlyNavigation/blob/master/fly-navigation/src/main/java/com/tiamosu/navigation/ext/FlyNavigationExt.kt)
- [FlyViewModelExt](https://gitee.com/tiamosu/FlyNavigation/blob/master/fly-navigation/src/main/java/com/tiamosu/navigation/ext/FlyViewModelExt.kt)

#### 4、依赖库

```gradle
androidx.appcompat:appcompat:1.6.1
androidx.core:core-ktx:1.10.1
androidx.fragment:fragment:1.6.1
androidx.navigation:navigation-runtime:2.6.0
androidx.lifecycle:lifecycle-runtime:2.6.2
androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.2
androidx.lifecycle:lifecycle-livedata-ktx:2.6.2
```

### fly-databinding 模块

#### 1、继承

```kotlin
abstract class BaseActivity : FlyDataBindingActivity() 
```

```kotlin
abstract class BaseFragment : FlyDataBindingFragment() 
```

#### 2、获取DataBinding

A、通过 `by lazyDataBinding()` 懒加载获取

```kotlin
private val dataBinding: MyDataBinding by lazyDataBinding()
```

B、通过 `View.toDataBinding` 进行获取

```kotlin
val dataBinding: MyViewDataBinding = view.toDataBinding()
```

## Thanks to

* [Smooth-Navigation](https://github.com/KunMinX/Smooth-Navigation)
* [SFragmentation](https://github.com/weikaiyun/SFragmentation)
* [ViewBindingPropertyDelegate](https://github.com/kirich1409/ViewBindingPropertyDelegate)