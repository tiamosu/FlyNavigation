@file:Suppress("unused", "SpellCheckingInspection")

object Android {
    const val compileSdk = 33
    const val minSdk = 21
    const val targetSdk = 33

    const val versionName = "1.0"
    const val versionCode = 1
}

object Versions {
    const val kotlin = "1.8.20"
    const val leakcanary = "2.12"
}

object Deps {
    //androidx
    const val core = "androidx.core:core-ktx:1.10.1"
    const val annotation = "androidx.annotation:annotation:1.7.0"
    const val appcompat = "androidx.appcompat:appcompat:1.6.1"
    const val viewpager2 = "androidx.viewpager2:viewpager2:1.0.0"
    const val constraintlayout = "androidx.constraintlayout:constraintlayout:2.1.4"
    const val recyclerview = "androidx.recyclerview:recyclerview:1.3.1"

    //navigation
    const val fragment = "androidx.fragment:fragment:1.6.1"
    const val navigation_runtime = "androidx.navigation:navigation-runtime:2.6.0"

    //lifecycle
    const val lifecycle_runtime = "androidx.lifecycle:lifecycle-runtime:2.6.2"
    const val lifecycle_viewmodel_ktx = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.2"
    const val lifecycle_livedata_ktx = "androidx.lifecycle:lifecycle-livedata-ktx:2.6.2"

    //leakcanary：内存泄漏检测 https://github.com/square/leakcanary
    const val leakcanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakcanary}"
}