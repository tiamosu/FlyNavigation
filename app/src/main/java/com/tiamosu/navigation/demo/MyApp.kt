package com.tiamosu.navigation.demo

import com.tiamosu.navigation.FlyApplication
import com.tiamosu.navigation.demo.domain.message.SharedViewModel
import com.tiamosu.navigation.ext.lazyAppViewModel

/**
 * @author tiamosu
 * @date 2021/5/11.
 */

val sharedViewModel by lazyAppViewModel<SharedViewModel>()

@Suppress("unused")
class MyApp : FlyApplication()