package com.tiamosu.navigation.demo.base

import android.os.Bundle
import com.tiamosu.databinding.page.FlyDataBindingActivity

/**
 * @author tiamosu
 * @date 2021/5/7.
 */
abstract class BaseActivity : FlyDataBindingActivity() {

    abstract fun doBusiness()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        doBusiness()
    }
}