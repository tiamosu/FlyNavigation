package com.tiamosu.navigation.demo.ui.activities

import android.util.Log
import com.tiamosu.databinding.delegate.lazyDataBindingOrNull
import com.tiamosu.databinding.page.DataBindingConfig
import com.tiamosu.navigation.demo.R
import com.tiamosu.navigation.demo.base.BaseActivity
import com.tiamosu.navigation.demo.databinding.ActivityMainBinding
import com.tiamosu.navigation.utils.FlyUtils

class MainActivity : BaseActivity() {
    private val dataBinding: ActivityMainBinding? by lazyDataBindingOrNull()

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.activity_main)
    }

    override fun doBusiness() {
        Log.e("susu", "dataBinding:$dataBinding   getApp:${FlyUtils.getApp()}")
    }

    override fun onFlySupportVisible() {
        super.onFlySupportVisible()
        Log.e("susu", "onFlySupportVisible:${isFlySupportVisible()}")
    }

    override fun onFlySupportInvisible() {
        super.onFlySupportInvisible()
        Log.e("susu", "onFlySupportInvisible:${isFlySupportVisible()}")
    }
}