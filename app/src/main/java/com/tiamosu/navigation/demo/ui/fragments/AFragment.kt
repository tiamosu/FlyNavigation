package com.tiamosu.navigation.demo.ui.fragments

import com.tiamosu.databinding.delegate.lazyDataBinding
import com.tiamosu.databinding.page.DataBindingConfig
import com.tiamosu.navigation.demo.R
import com.tiamosu.navigation.demo.base.BaseFragment
import com.tiamosu.navigation.demo.databinding.FragmentABinding
import com.tiamosu.navigation.demo.ext.jumpFragment

/**
 * @author tiamosu
 * @date 2021/5/7.
 */
class AFragment : BaseFragment() {
    private val dataBinding: FragmentABinding by lazyDataBinding()

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.fragment_a)
    }

    override fun doBusiness() {
        dataBinding.aBtnJumpD.setOnClickListener {
            jumpFragment(R.id.dFragment, singleTop = true)
        }
    }
}
