package com.tiamosu.navigation.demo.domain.message

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * @author tiamosu
 * @date 2021/5/11.
 */
class SharedViewModel : ViewModel() {

    //主页面 Tab 切换页面
    val selectTabItem by lazy { MutableLiveData<Class<out Fragment>>() }
}