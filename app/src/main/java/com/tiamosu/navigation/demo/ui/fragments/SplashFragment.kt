package com.tiamosu.navigation.demo.ui.fragments

import com.tiamosu.databinding.delegate.lazyDataBinding
import com.tiamosu.databinding.page.DataBindingConfig
import com.tiamosu.navigation.demo.R
import com.tiamosu.navigation.demo.base.BaseFragment
import com.tiamosu.navigation.demo.databinding.FragmentSplashBinding
import com.tiamosu.navigation.demo.ext.jumpFragment

/**
 * @author tiamosu
 * @date 2021/5/8.
 */
class SplashFragment : BaseFragment() {
    private val dataBinding: FragmentSplashBinding by lazyDataBinding()

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.fragment_splash)
    }

    override fun doBusiness() {
        dataBinding.splashBtnJumpMain.setOnClickListener {
            jumpFragment(R.id.mainFragment)
        }
    }
}