package com.tiamosu.navigation.demo.ui.fragments

import android.util.Log
import com.tiamosu.databinding.delegate.lazyDataBinding
import com.tiamosu.databinding.page.DataBindingConfig
import com.tiamosu.navigation.demo.R
import com.tiamosu.navigation.demo.base.BaseFragment
import com.tiamosu.navigation.demo.databinding.FragmentDBinding
import com.tiamosu.navigation.demo.ext.jumpFragment
import com.tiamosu.navigation.demo.sharedViewModel

/**
 * @author tiamosu
 * @date 2021/5/7.
 */
class DFragment : BaseFragment() {
    private val dataBinding: FragmentDBinding by lazyDataBinding {
        Log.e("susu", "onViewDestroyed")
    }

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.fragment_d)
    }

    override fun doBusiness() {
        dataBinding.dBtnJumpTest.setOnClickListener {
            jumpFragment(R.id.dFragment, singleTop = true)
        }

        dataBinding.dBtnSharedViewModelUse.setOnClickListener {
            sharedViewModel.selectTabItem.value = BFragment::class.java
            jumpFragment(popUpToId = R.id.mainFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.e("susu", "onDestroyView")
    }
}