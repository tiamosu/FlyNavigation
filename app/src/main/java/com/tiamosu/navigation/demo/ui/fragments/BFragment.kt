package com.tiamosu.navigation.demo.ui.fragments

import com.tiamosu.databinding.page.DataBindingConfig
import com.tiamosu.navigation.demo.R
import com.tiamosu.navigation.demo.base.BaseFragment

/**
 * @author tiamosu
 * @date 2021/5/7.
 */
class BFragment : BaseFragment() {

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.fragment_b)
    }

    override fun doBusiness() {
    }
}