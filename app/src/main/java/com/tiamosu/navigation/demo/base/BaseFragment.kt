package com.tiamosu.navigation.demo.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import com.tiamosu.databinding.page.FlyDataBindingFragment

/**
 * @author tiamosu
 * @date 2021/5/7.
 */
abstract class BaseFragment : FlyDataBindingFragment() {

    protected abstract fun doBusiness()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e(fragmentTag, "onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(fragmentTag, "onCreate")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.e(fragmentTag, "onCreateView")
        super.onViewCreated(view, savedInstanceState)
        Log.e(fragmentTag, "onViewCreated")
    }

    @Deprecated("Deprecated in Java")
    @Suppress("DEPRECATION")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.e(fragmentTag, "onActivityCreated")
    }

    override fun onResume() {
        Log.e(fragmentTag, "onResume")
        super.onResume()
    }

    override fun onPause() {
        Log.e(fragmentTag, "onPause")
        super.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.e(fragmentTag, "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(fragmentTag, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.e(fragmentTag, "onDetach")
    }

    override fun onFlyLazyInitView2() {
        super.onFlyLazyInitView2()
        Log.e(fragmentTag, "onFlyLazyInitView")
        doBusiness()
    }

    override fun onFlySupportVisible() {
        super.onFlySupportVisible()
        Log.e(fragmentTag, "onFlySupportVisible")
    }

    override fun onFlySupportInvisible() {
        super.onFlySupportInvisible()
        Log.e(fragmentTag, "onFlySupportInvisible")
    }
}