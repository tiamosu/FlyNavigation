package com.tiamosu.navigation.delegate

/**
 * @author tiamosu
 * @date 2020/4/15.
 */
interface IFlySupportActivity : IFlyVisibleCallback {

    fun getSupportDelegate(): FlySupportActivityDelegate

    fun onBackPressed()

    fun onBackPressedSupport()
}