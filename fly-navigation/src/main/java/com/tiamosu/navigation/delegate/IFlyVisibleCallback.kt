package com.tiamosu.navigation.delegate

/**
 * @author tiamosu
 * @date 2021/6/19
 */
interface IFlyVisibleCallback {

    fun isFlySupportVisible(): Boolean = false

    fun onFlySupportVisible() {}

    fun onFlySupportInvisible() {}
}