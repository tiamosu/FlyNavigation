package com.tiamosu.navigation.delegate

import androidx.fragment.app.Fragment

/**
 * @author tiamosu
 * @date 2020/5/12.
 */
class FlyVisibleDelegate(private val supportF: IFlySupportFragment) {
    private var isSupportVisible = false
    private var fragment: Fragment

    init {
        if (supportF !is Fragment) {
            error("${supportF.javaClass.simpleName} must impl Fragment")
        }
        fragment = supportF
    }

    fun onResume() {
        if (!fragment.isHidden && !isSupportVisible) {
            isSupportVisible = true
            supportF.onSupportVisible()
        }
    }

    fun onPause() {
        if (isSupportVisible) {
            isSupportVisible = false
            supportF.onSupportInvisible()
        }
    }

    fun onDestroyView() {
        isSupportVisible = false
    }

    fun isSupportVisible() = isSupportVisible
}