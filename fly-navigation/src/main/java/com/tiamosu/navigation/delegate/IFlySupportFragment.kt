package com.tiamosu.navigation.delegate

/**
 * @author tiamosu
 * @date 2020/4/15.
 */
interface IFlySupportFragment : IFlyVisibleCallback {

    fun getSupportDelegate(): FlySupportFragmentDelegate

    fun onSupportVisible()

    fun onSupportInvisible()

    fun onFlyLazyInitView()

    fun onFlyLazyInitView2()

    fun onBackPressedSupport(): Boolean
}