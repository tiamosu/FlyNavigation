package com.tiamosu.navigation.page

import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import com.tiamosu.navigation.delegate.FlySupportActivityDelegate
import com.tiamosu.navigation.delegate.IFlySupportActivity

/**
 * @author tiamosu
 * @date 2020/4/15.
 */
open class FlySupportActivity : AppCompatActivity(), IFlySupportActivity {
    private val delegate by lazy { FlySupportActivityDelegate(this) }
    private var isResumed = false

    override fun getSupportDelegate() = delegate

    override fun onResume() {
        isResumed = true
        super.onResume()
        onFlySupportVisible()
    }

    override fun onPause() {
        isResumed = false
        super.onPause()
        onFlySupportInvisible()
    }

    /**
     * [FlySupportActivity] 对用户可见时
     */
    @CallSuper
    override fun onFlySupportVisible() {
    }

    /**
     * [FlySupportActivity] 对用户不可见时
     */
    @CallSuper
    override fun onFlySupportInvisible() {
    }

    /**
     * [FlySupportActivity] 对用户可见判断
     */
    override fun isFlySupportVisible() = isResumed

    /**
     * 不建议复写该方法，请使用 [onBackPressedSupport] 代替
     */
    override fun onBackPressed() {
        delegate.onBackPressed()
    }

    /**
     * 该方法回调时机为，Activity 回退栈内 Fragment 的数量小于等于1时，默认 finish Activity
     * 请尽量复写该方法，避免复写 [onBackPressed]，以保证 [FlySupportFragment]
     * 内的 [FlySupportFragment.onBackPressedSupport] 回退事件正常执行
     */
    override fun onBackPressedSupport() {
        delegate.onBackPressedSupport()
    }
}