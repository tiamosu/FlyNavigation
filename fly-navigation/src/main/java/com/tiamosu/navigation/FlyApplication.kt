package com.tiamosu.navigation

import android.app.Application

/**
 * @author tiamosu
 * @date 2021/5/9
 */
open class FlyApplication : Application()