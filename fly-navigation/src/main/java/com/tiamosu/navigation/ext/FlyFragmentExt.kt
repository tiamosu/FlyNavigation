package com.tiamosu.navigation.ext

import androidx.fragment.app.Fragment

/**
 * @author tiamosu
 * @date 2022/3/12
 */

/**
 * Fragment 是否处于活动状态
 */
inline val Fragment.isAlive: Boolean
    get() = isAdded && !isDetached