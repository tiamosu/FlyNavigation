package com.tiamosu.navigation.ext

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner

/**
 * @author tiamosu
 * @date 2021/9/1.
 */

/**
 * 获取LifecycleOwner
 */
inline val LifecycleOwner.lifecycleOwnerEx: LifecycleOwner
    get() = when (this) {
        is Fragment -> {
            try {
                this.viewLifecycleOwner
            } catch (e: IllegalStateException) {
                this
            }
        }
        else -> this
    }

/**
 * 获取LifecycleOwner
 */
@Deprecated(message = "Use lifecycleOwnerEx", replaceWith = ReplaceWith("lifecycleOwnerEx"))
inline val LifecycleOwner.getLifecycleOwner: LifecycleOwner
    get() = lifecycleOwnerEx