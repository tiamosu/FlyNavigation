package com.tiamosu.navigation.ext

import androidx.lifecycle.ViewModel
import com.tiamosu.navigation.scope.ApplicationInstance

/**
 * 获取 Application 级别的 ViewModel
 */
inline fun <reified VM : ViewModel> lazyAppViewModel(): Lazy<VM> {
    return lazy {
        ApplicationInstance.getAppViewModelProvider()[VM::class.java]
    }
}