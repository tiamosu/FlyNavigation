package com.tiamosu.databinding.delegate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.tiamosu.navigation.utils.FlyUtils

/**
 * @author tiamosu
 * @date 2021/5/10.
 */

/**
 * Create new [ViewDataBinding] associated with the [View]
 */
fun <T : ViewDataBinding> View.toDataBinding(): T? {
    return DataBindingUtil.bind(this)
}

/**
 * Create new [ViewDataBinding] associated with the layoutId
 */
fun <T : ViewDataBinding> @receiver:IdRes Int.toDataBinding(
    parent: ViewGroup? = null,
    attachToParent: Boolean = false
): T? {
    return DataBindingUtil.inflate(
        LayoutInflater.from(FlyUtils.getApp()),
        this,
        parent,
        attachToParent
    )
}