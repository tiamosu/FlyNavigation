package com.tiamosu.databinding.delegate

import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import com.tiamosu.databinding.internal.emptyVbCallback
import com.tiamosu.databinding.internal.requireViewByIdCompat

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    crossinline vbFactory: (ViewGroup) -> T,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return lazyDataBindingOrNull(lifecycleAware = false, vbFactory)
}

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 * @param lifecycleAware Get [LifecycleOwner] from the [ViewGroup][this] using [ViewTreeLifecycleOwner]
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    lifecycleAware: Boolean,
    crossinline vbFactory: (ViewGroup) -> T,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return lazyDataBindingOrNull(lifecycleAware, vbFactory, emptyVbCallback())
}

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 * @param lifecycleAware Get [LifecycleOwner] from the [ViewGroup][this] using [ViewTreeLifecycleOwner]
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    lifecycleAware: Boolean,
    crossinline vbFactory: (ViewGroup) -> T,
    noinline onViewDestroyed: (T) -> Unit,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return when {
        isInEditMode -> EagerViewBindingPropertyOrNull(vbFactory(this))
        lifecycleAware -> ViewGroupViewBindingPropertyOrNull(onViewDestroyed) { viewGroup ->
            vbFactory(viewGroup)
        }
        else -> LazyViewBindingPropertyOrNull(onViewDestroyed) { viewGroup -> vbFactory(viewGroup) }
    }
}

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 * @param viewBindingRootId Root view's id that will be used as root for the view binding
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    crossinline vbFactory: (View) -> T,
    @IdRes viewBindingRootId: Int,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return lazyDataBindingOrNull(viewBindingRootId, vbFactory, emptyVbCallback())
}

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 * @param viewBindingRootId Root view's id that will be used as root for the view binding
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    @IdRes viewBindingRootId: Int,
    crossinline vbFactory: (View) -> T,
    noinline onViewDestroyed: (T) -> Unit,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return lazyDataBindingOrNull(
        viewBindingRootId,
        lifecycleAware = false,
        vbFactory,
        onViewDestroyed
    )
}

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 * @param viewBindingRootId Root view's id that will be used as root for the view binding
 * @param lifecycleAware Get [LifecycleOwner] from the [ViewGroup][this] using [ViewTreeLifecycleOwner]
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    @IdRes viewBindingRootId: Int,
    lifecycleAware: Boolean,
    crossinline vbFactory: (View) -> T,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return lazyDataBindingOrNull(viewBindingRootId, lifecycleAware, vbFactory, emptyVbCallback())
}

/**
 * Create new [ViewBinding] associated with the [ViewGroup]
 *
 * @param vbFactory Function that create new instance of [ViewBinding]. `MyViewBinding::bind` can be used
 * @param viewBindingRootId Root view's id that will be used as root for the view binding
 * @param lifecycleAware Get [LifecycleOwner] from the [ViewGroup][this] using [ViewTreeLifecycleOwner]
 */
inline fun <T : ViewBinding> ViewGroup.lazyDataBindingOrNull(
    @IdRes viewBindingRootId: Int,
    lifecycleAware: Boolean,
    crossinline vbFactory: (View) -> T,
    noinline onViewDestroyed: (T) -> Unit,
): ViewBindingPropertyOrNull<ViewGroup, T?> {
    return when {
        isInEditMode -> EagerViewBindingPropertyOrNull(vbFactory(this))
        lifecycleAware -> ViewGroupViewBindingPropertyOrNull(onViewDestroyed) { viewGroup ->
            vbFactory(viewGroup)
        }
        else -> LazyViewBindingPropertyOrNull(onViewDestroyed) { viewGroup: ViewGroup ->
            vbFactory(viewGroup.requireViewByIdCompat(viewBindingRootId))
        }
    }
}