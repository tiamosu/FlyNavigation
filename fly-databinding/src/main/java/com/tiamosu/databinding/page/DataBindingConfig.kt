package com.tiamosu.databinding.page

import android.util.SparseArray

/**
 * @author tiamosu
 * @date 2021/5/7.
 */
class DataBindingConfig(val layoutId: Int) {
    val bindingParams: SparseArray<Any> by lazy { SparseArray<Any>() }

    fun addBindingParam(variableId: Int, `object`: Any): DataBindingConfig {
        if (bindingParams[variableId] == null) {
            bindingParams.put(variableId, `object`)
        }
        return this
    }
}